
public class Cliente extends Cafeteria implements RegistroPuntos {
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(int idVenta, double importe) {
		super(idVenta, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		return this.getImporte();
	}

	@Override
	public String registrarPunto(int numVenta, String nombre) {
		String mensaje = "";
		int contador = 0;

		for (PuntosCafeteria elemento : Main.puntosCafeteria) {
			if (elemento.getNombre().equals(nombre)) {
				contador += elemento.getTotalPuntos();
			}
		}
		
		if(contador == 0) {
			// Registra el punto con la persona
			PuntosCafeteria objNuevo = new PuntosCafeteria(numVenta, nombre, "cliente", 100);
			Main.puntosCafeteria.add(objNuevo);
		} else {
			// Actualiza la información
			for(PuntosCafeteria elemento : Main.puntosCafeteria) {
				if(elemento.getNombre().equals(nombre)) {
					elemento.setTotalPuntos(contador + 10);
				}
			}
		}

		return mensaje;
	}

}
