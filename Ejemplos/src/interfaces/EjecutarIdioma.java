package interfaces;

public class EjecutarIdioma {

	public static void main(String[] args) {
		Spanish spanish = new Spanish();
		English english = new English();
		
		english.saludar();
		spanish.saludar();
	}

}
