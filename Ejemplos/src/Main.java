import java.util.ArrayList;
import java.util.List;

public class Main {
	public static final List<PuntosCafeteria> puntosCafeteria = new ArrayList<PuntosCafeteria>();
	public static void main(String[] args) {
		Cliente c = new Cliente(1, 100);
		// System.out.println("Total a pagar: " + c.cobrar());
		
		Trabajador t = new Trabajador(2, 100);
		// System.out.println("Total a pagar: " + t.cobrar());
		
		ejecutarPago(c);
		ejecutarPago(t);
		
		c.registrarPunto(1, "Juan Gabriel");
	}

	public static void ejecutarPago(Cafeteria cafeteria) {
		System.out.println("Total a pagar: " + cafeteria.cobrar());
	}
}
